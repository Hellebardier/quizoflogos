package com.ch.brunner.adrian.quizoflogos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class Menu extends AppCompatActivity {

    Button btnBank, btnAmerican;
    SensorManager sensorManager;
    Sensor sensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        btnBank = (Button) findViewById(R.id.btnBank);
        btnAmerican = (Button) findViewById(R.id.btnAmerican);

        sensorManager = (SensorManager) getSystemService(Service.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

       btnBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMainActivity();
            }
        });

        btnBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMainActivity();
            }
        });

        btnAmerican.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAmericanActivity();
            }
        });
    }

    public void openMainActivity(){
        Intent absicht = new Intent(this,  MainActivity.class);
        startActivity(absicht);
        }

    public void openAmericanActivity(){
        Intent sicht = new Intent(this,  American.class);
        startActivity(sicht);
    }

   /* @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(lightEventListener, lightSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }
    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(lightEventListener);
    }

    lightEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            RelativeLayout r1 = (RelativeLayout)findViewById(R.id.constraint);
            r1.setBackgroundColor(Color.rgb(0, 0, 0));
        }

    public void onSensorChanged(SensorEvent event){
        if(event.sensor.getType() == Sensor.TYPE_LIGHT){
            ConstraintLayout b4 = (ConstraintLayout) findViewById(R.id.constraint);
            b4.setBackgroundColor(Color.rgb(0,0,0));
        }
    }*/
}