package com.ch.brunner.adrian.quizoflogos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnMenue, btnCheck;
    ImageView iVLogo;
    EditText nameLogo;

    LottieAnimationView av_correct;

    int currentLevel;
    final int maxLevel = 4;
    String companyName, imageName;

    final String prefNameFirstStart = "firstAppStart";
    final String databaseName = "level.db";
    final String databaseTableName = "level";
    final String prefLevel = "currentLevel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iVLogo = findViewById(R.id.iVLogo);

        nameLogo = findViewById(R.id.nameLogo);

        av_correct = findViewById(R.id.av_correct);

        btnMenue = findViewById(R.id.btnMenue);
        btnCheck = findViewById(R.id.btnCheck);

        btnMenue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMenue();
            }
        });

        btnCheck.setOnClickListener(this);


        if (firstAppStart()){
            createDatabase();
        }

        loadLevel();
    }

        public void openMenue(){
        Intent intent = new Intent(this, Menu.class);
        startActivity(intent);
        }

        public void loadLevel(){
            SharedPreferences preferencesLoad = getSharedPreferences(prefLevel, MODE_PRIVATE);
            currentLevel = preferencesLoad.getInt(prefLevel, 1);
            if (currentLevel <= maxLevel){
                SQLiteDatabase database = openOrCreateDatabase(databaseName, MODE_PRIVATE, null);
                Cursor cursor = database.rawQuery("SELECT * FROM " + databaseTableName + " WHERE id = '" + currentLevel + "'", null);
                cursor.moveToFirst();
                if (cursor.getCount() == 1){
                    companyName = cursor.getString(1);
                    imageName = cursor.getString(2);
                    cursor.close();
                    database.close();
                }
                int imgID = getResources().getIdentifier(imageName, "drawable", getPackageName());
                iVLogo.setImageResource(imgID);
            }
            animateLevelCompleted();
        }

        public void safeLevel(){
            SharedPreferences preferencesLevel = getSharedPreferences(prefLevel, MODE_PRIVATE);
            SharedPreferences.Editor editor = preferencesLevel.edit();

            editor.putInt(prefLevel, currentLevel);
            editor.commit();
        }

        public void animateLevelCompleted(){
            iVLogo.setVisibility(View.INVISIBLE);
            btnCheck.setVisibility(View.INVISIBLE);
            btnMenue.setVisibility(View.INVISIBLE);
            nameLogo.setVisibility(View.INVISIBLE);

            av_correct.playAnimation();
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(1000);

            btnMenue.setVisibility(View.VISIBLE);
        }

        public boolean firstAppStart(){
            SharedPreferences preferences = getSharedPreferences(prefNameFirstStart, MODE_PRIVATE);
            if (preferences.getBoolean(prefNameFirstStart, true)){
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(prefNameFirstStart,false);
                editor.commit();
                return false;
            } else {
                return false;
            }
        }

        public void createDatabase(){
            SQLiteDatabase db = openOrCreateDatabase(databaseName, MODE_PRIVATE, null);
            db.execSQL("CREATE TABLE " + databaseTableName + " (id INTEGER, company TEXT, imageName TEXT)");
            db.execSQL("INSERT INTO " + databaseTableName + " VALUES('1','UBS', 'ubs')");
            db.execSQL("INSERT INTO " + databaseTableName + " VALUES('2','Santander', 'santander')");
            db.execSQL("INSERT INTO " + databaseTableName + " VALUES('3','Vontobel', 'vontobel')");
            db.execSQL("INSERT INTO " + databaseTableName + " VALUES('4','CreditSuisse', 'cs')");
            db.close();
        //Writeable Tamino mit Marko
        SQLiteDatabase db = this.getWriteable;
        db.execSQL("DROP TABLE " + databaseTableName);
        }

    /*public void dropDatabase(){
    }*/

        @Override
        public void onClick(View view){
            switch (view.getId()){
                case R.id.btnCheck:
                     if (nameLogo.getText().toString().equalsIgnoreCase(companyName)){
                         if (currentLevel == maxLevel){
                             animateLevelCompleted();
                             currentLevel = 0;
                             //dropDatabase();
                         } else {
                             currentLevel++;
                         }
                        safeLevel();
                        loadLevel();
                    } else {
                        Toast.makeText(getApplicationContext(), "Falsch", Toast.LENGTH_LONG).show();
                    }
                    break;
                case R.id.btnMenue:

                    break;
            }
        }

}